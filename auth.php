<?php

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Error\ClientAware;

class PE_GraphQL_Exception extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'No access';
    }
}
class PE_GraphQL_Attention_Exception extends \Exception implements ClientAware
{
	public function isClientSafe()
    {
        return true;
    }
    public function getCategory()
    {
        return 'ATTENTION';
    }
}
add_filter("pegq_get_tocken", function($user_id, $jwt_token)
{
	$is_blocked = get_user_meta($user_id, "is_blocked", true);
	if($is_blocked)
	{
		//throw new PE_GraphQL_Exception ("...");
		//return null;
	}
	return $user_id;
});
/*
add_filter("pe_graphql_get_user", function( $user )
{
	$u = [];
	$u['id'] = $user->ID;
	$u['ID'] = $user->ID;
	$u['first_name'] = $user->first_name;
	$u['last_name'] = $user->last_name;
	$u['display_name'] = $user->display_name;
	$u['roles'] = $user->roles;
	$u['user_login'] = $user->user_login;
	$u['user_email'] = $user->user_email;
	return $u;
}, 1);
*/

add_action("pe_graphql_make_schema", function() 
{
    PEGraphql::add_object_type([
        'name' => 'User',
        'fields' => apply_filters(
			"pe_graphql_user_fields", 
			[
				'id' => Type::string(),
				'user_login' => Type::string(),
				'user_email' => Type::string(),
				'display_name' => Type::string(),
				'first_name' => Type::string(),
				'last_name' => Type::string(),
				'user_descr' => Type::string(),
				'avatar' => Type::string(),
				'avatar_name' => Type::string(),
				'roles' => [
					'type' => Type::listOf(Type::string()),
				]
			],
			false
		),
        
    ]);

    PEGraphql::add_query(
		"getUser",  
		[
			'type' => PEGraphql::object_type("User"),
			'args' => [
				'id' => Type::string()
			],
			'resolve' => function ($root, $args) {
				$user = get_user_by("id", $args["id"]);
				if ($user->ID > 0) 
				{
					$user->id = $user->ID;
					return apply_filters("pe_graphql_get_user", $user);
				}
			}
		]
	);
    PEGraphql::add_query(
		"getUserCount",  
		[
			'type' => Type::int(),
			'args' => [ 
				"paging" 	=> [ "type" => PEGraphql::input_type("UserPaging") ],
			],
			'resolve' => function ($root, $args) {
				$users  = get_users(['blog_id' => $GLOBALS['blog_id']]);
				return count( $users );
			}
		]
	);

    PEGraphql::add_query(
		"getUsers",  
		[
			'type' => Type::listOf(PEGraphql::object_type("User")),
			'args' => [ 
				"paging" 	=> [ "type" => PEGraphql::input_type("UserPaging") ],
			],
			'resolve' => function ($root, $args) {
				$users  = get_users(['blog_id' => $GLOBALS['blog_id']]);
				return  apply_filters("pe_graphql_get_users", $users);
			}
		]
	);

    PEGraphql::add_input_type([
        'name' => 'UserInput',
        'fields' =>  apply_filters(
			"pe_graphql_user_fields", 
			[
				'id' => Type::string(),
				'user_login' => Type::string(),
				'user_email' => Type::string(),
				'display_name' => Type::string(),
				'first_name' => Type::string(),
				'last_name' => Type::string(),
				'user_descr' => Type::string(),
				'avatar' => Type::string(),
				'avatar_name' => Type::string(),
				'password' => Type::string(),
				'roles' => [
					'type' => Type::listOf(Type::string()),
				]
			], 
			true
		),
    ]);
	
	PEGraphql::add_mutation( 
		"changeUser",
		[
			'description' 	=> __( "Change User", BIO ),
			'type' 			=> PEGraphql::object_type("User"),
			'args'         	=> [
				"id"	=> [
					'type' => Type::string(),
					'description' => __( 'Unique identificator of User', BIO ),
				],
				'input' => [
					'type' => PEGraphql::input_type('UserInput'),
					'description' => __( "User's new params", BIO ),
				]
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( !$args[ 'id' ] )
				{	
					$user_id =  wp_insert_user([ 
						"user_login"	=> $args['input'][ 'user_login' ], 
						"first_name"	=> $args['input'][ 'first_name' ], 
						"display_name"	=> $args['input'][ 'display_name' ], 
						"last_name"		=> $args['input'][ 'last_name' ], 
						"user_descr"	=> $args['input'][ 'user_descr' ], 
						"description"	=> $args['input'][ 'description' ], 
						"user_pass"		=> $args['input'][ 'password' ], 
						"user_email"	=> $args['input'][ 'user_email' ] 
					]);					
					if ( is_wp_error( $user_id ) ) 
					{
						throw new PE_GraphQL_Exception( $user_id->get_error_message() );
						//wp_die( $user_id->get_error_message() );
					}
					else
					{
						if( isset( $args['input']['roles'] ) )
						{
							$user = get_user_by( "id", $user_id );					
							foreach( $args['input']['roles'] as $role )
							{
								$user->add_role( $role );
							}
						}
						else
						{
							$user = get_user_by( "id", $user_id );		
							$user->add_role("Contributor");	
						}
						return Bio_User::get_user( $user_id );
					}
				}
				
				else
				{					
					if( !current_user_can('edit_users', $args['id']) )
					{
						throw new PE_GraphQL_Exception ("you not rights");
					}
					$args['input']['ID']	= $args["id"];
					
					$user_id = wp_update_user(apply_filters(
						"pe_graphql_change_user",
						$args['input'], 
						$args['id']
					));
					$user_id = apply_filters("pe_graphql_change_meta_user", $user_id, $args);
					
					if( is_wp_error( $user_id ) )
					{
						throw new PE_GraphQL_Exception ("inknown error in edit proccess...");
					}
					if( isset( $args['input']['roles'] ) )
					{
						$user = get_user_by( "id", $args[ "id" ] );
						foreach( $user->roles as $role )
						{
							$user->remove_role( $role );
						}
						foreach( $args['input']['roles'] as $role )
						{
							$user->add_role( $role );
						}
					}
					return Bio_User::get_user( $user_id );
				}
				/**/
			}
		]
	);
	PEGraphql::add_mutation( 
		"changeCurrentUser",
		[
			'description' 	=> __( "Change Current User", BIO ),
			'type' 			=> Type::int(),
			'args'         	=> [
				'input' => [
					'type' => PEGraphql::input_type('UserInput'),
					'description' => __( "User's new params", BIO ),
				]
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( get_current_user_id() < 0 )
				{
					throw new PE_GraphQL_Exception ("you not rights");
					//wp_die( "you not rights" );
				}
				$user_id = wp_update_user( [ 
					'ID'       		=> get_current_user_id(), 
					'user_pass' 	=> $args['input']["password"] ? $args['input']["password"] : null, 
					'nickname' 		=> $args['input']["nickname"] ? $args['input']["nickname"] : null, 
					'first_name' 	=> $args['input']["first_name"] ? $args['input']["first_name"] : null, 
					'last_name' 	=> $args['input']["last_name"] ? $args['input']["last_name"] : null, 
					'user_descr' => $args['input']["user_descr"] ? $args['input']["user_descr"] : null, 
				] ); 
				
				if(
					isset( $args[ 'input' ][ "user_email" ] ) 
					&&	   $args[ 'input' ][ "user_email" ] != wp_get_current_user()->user_email
				)
				{
					// verify email field engine start
					$code 		= md5(time());
					update_user_meta( get_current_user_id(), 'new_email_code', $code);
					update_user_meta( get_current_user_id(), "new_user_email", $args[ 'input' ][ "user_email" ] );
					wp_mail(
						wp_get_current_user()->user_email,//"genglaz@gmail.com",
						sprintf( __("Процедура восстановления регистрации электронной почты на %s", BIO),  get_bloginfo("title") ),
						sprintf( 
							__("Вы можете сменить зарегистрированный адрес электронной почты. Для этого перейдите <a href='%s'>по этой ссылке</a>", BIO),  
							Bio::$options['web_client_url'] . "/changeemail/$user_id/$code" 
						),
						[ 'content-type: text/html' ]
					);
					return 2;
				}
				do_action("pe_graphql_change_current_user", $args);
				if( is_wp_error( $user_id ) )
				{
					throw new PE_GraphQL_Exception ("inknown error in edit proccess...");
				}
				return 1;
			}
		]
	);
	
		PEGraphql::add_input_type(
			[
				"name"		=> 'MetaFilter',
				'description' => __( "Filter by meta Field", BIO ),
				"fields"	=> [
					'key'	=> [
							'type' 	=> Type::string(),
							'description' => __( 'name of filter term', BIO )
						],
					'value'	=> [
							'type' 	=> Type::string() ,
							'description' => __( 'sting value', BIO )
						],
					'value_numeric'	=> [
							'type' 	=> Type::int() ,
							'description' => __( 'numeric value', BIO )
						],
					'value_bool'	=> [
							'type' 	=> Type::boolean() ,
							'description' => __( 'boolean value', BIO )
						],
					'compare'	=> [
							'type' 	=> Type::string() ,
							'description' => '=, !=, >, <, >=, <=, EXISTS, NOT EXISTS, IN, NOT IN, BETWEEN, NOT BETWEEN'
						]
				]
			]
		);	
		
		PEGraphql::add_input_type( 
			[
				"name"		=> 'UserPaging',
				'description' => __( "Pagination of Users collection", BIO ),
				'fields' 		=> [
					'count' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'Count of elements in page. Default - 10', BIO ),
						"defaultValue" => 10000
					],
					'offset' 	=> [
						'type' 	=> Type::int(),
						'description' => __( 'Current page. Default is 1', BIO ),
						"defaultValue" => 0
					],
					"meta_relation" => [
						'type' 	=> Type::string(),
						'description' => __( 'Comare of different meta filters', BIO ),
						"defaultValue" => "OR"
					],
					"order" => [
						'type' 	=> Type::string(),
						'description' => __( 'Current page. Default is 1', BIO ),
						"defaultValue" => 'date'
					],
					'order_by_meta'	=> [
						'type' 	=> Type::string(),
						'description' => __( 'Current page. Default is 1', BIO ),
						"defaultValue" => ''
					],
					'relation'		=> [
						'type' 	=> Type::string(),
						'description' => __( 'Current page. Default is 1', BIO ),
					],
					'metas'	=> [
						'type' 	=> Type::listOf(PEGraphql::input_type("MetaFilter")),//,
						'description' => __( ' ', BIO ),
						"defaultValue" => []
					],
					'is_admin'	=> [
						'type' 	=> Type::boolean(),
						'description' => __( 'For admin panel', BIO ),
						"defaultValue" => false
					],
				],
			]
		);
	//register
	PEGraphql::add_mutation( 
		'verifyEmailUser', 
		[
			'description' 	=> __( "Verified User after create account", BIO ),
			'type' 			=> Type::string(),
			'args'         	=> [
				'id' 	=> [
					'type' 	=> Type::string(),
					'description' => __( 'User unique identificator', BIO )
				],
				'code' => [
					'type' => Type::string(),
					'description' => __( "Unique cache that user have in registration e-mail.", BIO ),
				]
			],
			'resolve' => function( $root, $args, $context, $info )
			{
				$code				= $args['code'];
				$new_email_code	= get_user_meta((int)$args['id'], 'new_email_code', $true);
				$new_user_email	= get_user_meta((int)$args['id'], 'new_user_email', $true);
				if($new_email_code[0] == $code)
				{
					$user_id = wp_update_user( [ 
						'ID'       		=> (int)$args['id'], 
						'user_email' 	=> $new_user_email[0]
					] );
					delete_user_meta((int)$args['id'], 'new_email_code' );
					return $new_user_email[0];
				}
				else
				{
					return "";
				}
				
			}
		]
	);
	
	
	
	PEGraphql::add_mutation( 
		"deleteUser",
		[
			'description' 	=> __( "Delete User account", BIO ),
			'type' 			=> Type::boolean(),
			'args'         	=> [
				"id"	=> [
					'type' => Type::string(),
					'description' => __( 'Unique identificator of User', BIO ),
				]
			],
			'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
			{		
				require_once ABSPATH . 'wp-admin/includes/user.php';
				if( !current_user_can('delete_users' ) )
				{
					throw new PE_GraphQL_Exception ("you not rights");
					//wp_die( "you not rights");
				}
				
				return wp_delete_user( $args['id'] );
			}
		]
	);
	
    PEGraphql::add_input_type([
        'name' => 'TokenInput',
        'fields' => [
            'grant_type' => [
                'type' => Type::string(),
            ],
            'login' => [
                'type' => Type::string(),
            ],
            'password' => [
                'type' => Type::string(),
            ],
        ]
    ]);

    PEGraphql::add_object_type([
        'name' => 'UserToken',
        'fields' => [
            'access_token' => [
                'type' => Type::string(),
            ],
            'token_type' => [
                'type' => Type::string(),
            ],
            'expires_in' => [
                'type' => Type::string(),
            ],
            'refresh_token' => [
                'type' => Type::string(),
            ],
        ]
    ]);

    PEGraphql::add_query("userInfo",  [
        'type' => PEGraphql::object_type("User"),
        'args' => [
            //'message' => Type::nonNull(PEGraphql::input_type("UserInput")),
        ],
        'resolve' => function ($root, $args) {
			$user = wp_get_current_user();
            if ($user->ID > 0) {
                $user->id = $user->ID;
                //$user->first_name = $user->first_name;
                //
				return apply_filters("pe_graphql_get_user", $user);
            }
			return null;
        }
    ]);

    PEGraphql::add_mutation("token",  [
        'type' => PEGraphql::object_type("UserToken"),
        'args' => [
            'input' => PEGraphql::input_type("TokenInput"),
        ],
        'resolve' => function ($root, $args) {
            $user = get_user_by('email', $args["input"]["login"]);
            if ( !wp_check_password( $args["input"]["password"], $user->user_pass ) ) {
                throw new PE_GraphQL_Exception ("Неверный пароль");
            }
			//blocked user
			$is_blocked			= get_user_meta($user->ID, "is_blocked", true);
			if($is_blocked)
			{
				throw new PE_GraphQL_Exception (__("Данный аккаунт заблокирован. Обращайтесь к администратору портала. Адрес электронной почты: ") . get_bloginfo("admin_email") );
			}
			// проверка на подтвержденный по почте аккаунт
			$account_activated = get_user_meta($user->ID, "account_activated", true);
			if(!$account_activated)
			{
				throw new PE_GraphQL_Exception ("Необходимо активировать Ваш аккаунт по инструкции, которую Вы получили на свой постовый адрес.");
			}
			
			
            $headers = array(
                'alg' => 'HS256', //alg is required. see *Algorithms* section for supported algorithms
                'typ' => 'JWT'
            );
            
            // anything that json serializable
            $payload = array(
                'sub' => $user->ID,
                'iat' => time(),
            );
            
            $key = GRAPHQL_JWT_AUTH_SECRET_KEY;
            
            $jws = new \Gamegos\JWS\JWS();
            return ["access_token" => $jws->encode($headers, $payload, $key)];
        }
    ]);
	
	
});